from django.contrib import admin

from models import Section, Author, Article, Comment

from .forms import ArticleAdminForm

class ArticleAdmin(admin.ModelAdmin):
    form = ArticleAdminForm
    list_display = ('title', 'active', 'date_creation', 'author', 'section', 'reads', )
    readonly_fields = ('reads', )

class CommentAdmin(admin.ModelAdmin):
    list_display = ('title', 'article', 'author_name', 'author_email', 'created_at', )


admin.site.register(Article, ArticleAdmin)
admin.site.register(Author)
admin.site.register(Section)
admin.site.register(Comment, CommentAdmin)
