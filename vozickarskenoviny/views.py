import datetime

from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.db.models import Sum

from models import Article, Section
from forms import CommentForm


def home_view(request, template="vozickarskenoviny/home.html"):
    last_from_section = Section.objects.with_article()
    for i in xrange(len(last_from_section)):
        last_from_section[i].last = last_from_section[i].last_article()
    last_from_section = zip(last_from_section[::2], last_from_section[1::2])

    reads_sum = Article.objects.all().aggregate(Sum('reads'))['reads__sum']
    return render_to_response(template,
        {
            "main_menu": Section.objects.with_article(),
            "popular3": Article.objects.popularity()[:3],

            'reads_sum': Article.objects.all().aggregate(Sum('reads'))['reads__sum'], 
            "last_from_section": last_from_section,
            "last": Article.objects.last(),
            "last3": Article.objects.all()[1:4],
        },
        context_instance=RequestContext(request))


def section_view(request, url_key, template="vozickarskenoviny/section.html"):
    section = Section.objects.get(url_key=url_key)
    articles = Article.objects.all().filter(section=section)
    return render_to_response(template,
        {
            'title': section.name,
            "main_menu": Section.objects.with_article(),
            'current_section': section,
            "popular3": Article.objects.popularity()[:3],

            "last": articles[0],
            "next": articles[1:],
        },
        context_instance=RequestContext(request))

def article_view(request, url_key, template="vozickarskenoviny/article.html"):
    article = get_object_or_404(Article, url_key=url_key, date_creation__lte=datetime.datetime.now())
    article.reads += 1
    article.save()

    comment_form = CommentForm(request.POST or None)
    if comment_form.is_valid():
        comment = comment_form.save(commit=False)
        comment.article = article
        comment.save()
        return HttpResponseRedirect(reverse("vn.article", args=[article.url_key], ))

    return render_to_response(template,
        {
            'title': article.title,
            "main_menu": Section.objects.with_article(),
            "comment_form": comment_form,
            'current_section': article.section,
            "popular3": Article.objects.popularity()[:3],

            "article": article,
            "related1": article.get_related()[0:3],
            "related2": article.get_related()[4:7],
            "related3": article.get_related()[8:10],
        },
        context_instance=RequestContext(request))

def sitemap_view(request, template="vozickarskenoviny/sitemap.xml"):
    urls = ['/']
    for section in Section.objects.all():
        urls.append(reverse('vn.section', args=(section.url_key, )))
    for article in Article.objects.all():
        urls.append(reverse('vn.article', args=(article.url_key, )))
    return render_to_response(template,
        {
            "urls": urls,
        },
        content_type="text/xml",
        context_instance=RequestContext(request))
