# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Section'
        db.create_table(u'vozickarskenoviny_section', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('url_key', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=32, blank=True)),
            ('order', self.gf('django.db.models.fields.IntegerField')(default=1)),
        ))
        db.send_create_signal(u'vozickarskenoviny', ['Section'])

        # Adding model 'Author'
        db.create_table(u'vozickarskenoviny_author', (
            (u'user_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True, primary_key=True)),
        ))
        db.send_create_signal(u'vozickarskenoviny', ['Author'])

        # Adding model 'Article'
        db.create_table(u'vozickarskenoviny_article', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('reads', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('date_creation', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('date_actualization', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('show_actualization', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('author', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['vozickarskenoviny.Author'])),
            ('section', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['vozickarskenoviny.Section'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('url_key', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=50, blank=True)),
            ('perex', self.gf('django.db.models.fields.TextField')()),
            ('text1', self.gf('django.db.models.fields.TextField')()),
            ('text2', self.gf('django.db.models.fields.TextField')()),
            ('keywords', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('img', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('video', self.gf('django.db.models.fields.CharField')(max_length=16, null=True, blank=True)),
            ('img1', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('img1_author', self.gf('django.db.models.fields.CharField')(default='', max_length=64, blank=True)),
            ('img1_description', self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True)),
            ('img2', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('img2_author', self.gf('django.db.models.fields.CharField')(default='', max_length=64, blank=True)),
            ('img2_description', self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True)),
            ('img3', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('img3_author', self.gf('django.db.models.fields.CharField')(default='', max_length=64, blank=True)),
            ('img3_description', self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True)),
            ('img4', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('img4_author', self.gf('django.db.models.fields.CharField')(default='', max_length=64, blank=True)),
            ('img4_description', self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True)),
            ('img5', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('img5_author', self.gf('django.db.models.fields.CharField')(default='', max_length=64, blank=True)),
            ('img5_description', self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True)),
            ('img6', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('img6_author', self.gf('django.db.models.fields.CharField')(default='', max_length=64, blank=True)),
            ('img6_description', self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True)),
        ))
        db.send_create_signal(u'vozickarskenoviny', ['Article'])

        # Adding model 'Comment'
        db.create_table(u'vozickarskenoviny_comment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('article', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['vozickarskenoviny.Article'])),
            ('created_at', self.gf('django.db.models.fields.DateField')(auto_now_add=True, blank=True)),
            ('author_name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('author_email', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('text', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'vozickarskenoviny', ['Comment'])

        # Adding model 'AdUser'
        db.create_table(u'vozickarskenoviny_aduser', (
            (u'user_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True, primary_key=True)),
            ('address', self.gf('django.db.models.fields.TextField')()),
            ('ic', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('dic', self.gf('django.db.models.fields.CharField')(max_length=32)),
        ))
        db.send_create_signal(u'vozickarskenoviny', ['AdUser'])

        # Adding model 'Ad'
        db.create_table(u'vozickarskenoviny_ad', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('owner', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['vozickarskenoviny.AdUser'])),
            ('position', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('start', self.gf('django.db.models.fields.DateField')()),
            ('end', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'vozickarskenoviny', ['Ad'])

        # Adding model 'TextAd'
        db.create_table(u'vozickarskenoviny_textad', (
            (u'ad_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['vozickarskenoviny.Ad'], unique=True, primary_key=True)),
            ('text', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'vozickarskenoviny', ['TextAd'])

        # Adding model 'ImageAd'
        db.create_table(u'vozickarskenoviny_imagead', (
            (u'ad_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['vozickarskenoviny.Ad'], unique=True, primary_key=True)),
            ('img', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
        ))
        db.send_create_signal(u'vozickarskenoviny', ['ImageAd'])


    def backwards(self, orm):
        # Deleting model 'Section'
        db.delete_table(u'vozickarskenoviny_section')

        # Deleting model 'Author'
        db.delete_table(u'vozickarskenoviny_author')

        # Deleting model 'Article'
        db.delete_table(u'vozickarskenoviny_article')

        # Deleting model 'Comment'
        db.delete_table(u'vozickarskenoviny_comment')

        # Deleting model 'AdUser'
        db.delete_table(u'vozickarskenoviny_aduser')

        # Deleting model 'Ad'
        db.delete_table(u'vozickarskenoviny_ad')

        # Deleting model 'TextAd'
        db.delete_table(u'vozickarskenoviny_textad')

        # Deleting model 'ImageAd'
        db.delete_table(u'vozickarskenoviny_imagead')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'vozickarskenoviny.ad': {
            'Meta': {'object_name': 'Ad'},
            'end': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['vozickarskenoviny.AdUser']"}),
            'position': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'start': ('django.db.models.fields.DateField', [], {})
        },
        u'vozickarskenoviny.aduser': {
            'Meta': {'object_name': 'AdUser', '_ormbases': [u'auth.User']},
            'address': ('django.db.models.fields.TextField', [], {}),
            'dic': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'ic': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'vozickarskenoviny.article': {
            'Meta': {'object_name': 'Article'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'author': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['vozickarskenoviny.Author']"}),
            'date_actualization': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'date_creation': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'img1': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img1_author': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '64', 'blank': 'True'}),
            'img1_description': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'img2': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img2_author': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '64', 'blank': 'True'}),
            'img2_description': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'img3': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img3_author': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '64', 'blank': 'True'}),
            'img3_description': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'img4': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img4_author': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '64', 'blank': 'True'}),
            'img4_description': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'img5': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img5_author': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '64', 'blank': 'True'}),
            'img5_description': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'img6': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img6_author': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '64', 'blank': 'True'}),
            'img6_description': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'keywords': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'perex': ('django.db.models.fields.TextField', [], {}),
            'reads': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'section': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['vozickarskenoviny.Section']"}),
            'show_actualization': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'text1': ('django.db.models.fields.TextField', [], {}),
            'text2': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'url_key': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50', 'blank': 'True'}),
            'video': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'})
        },
        u'vozickarskenoviny.author': {
            'Meta': {'object_name': 'Author', '_ormbases': [u'auth.User']},
            u'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'vozickarskenoviny.comment': {
            'Meta': {'object_name': 'Comment'},
            'article': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['vozickarskenoviny.Article']"}),
            'author_email': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'author_name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'created_at': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'vozickarskenoviny.imagead': {
            'Meta': {'object_name': 'ImageAd', '_ormbases': [u'vozickarskenoviny.Ad']},
            u'ad_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['vozickarskenoviny.Ad']", 'unique': 'True', 'primary_key': 'True'}),
            'img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'})
        },
        u'vozickarskenoviny.section': {
            'Meta': {'object_name': 'Section'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'url_key': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '32', 'blank': 'True'})
        },
        u'vozickarskenoviny.textad': {
            'Meta': {'object_name': 'TextAd', '_ormbases': [u'vozickarskenoviny.Ad']},
            u'ad_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['vozickarskenoviny.Ad']", 'unique': 'True', 'primary_key': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['vozickarskenoviny']