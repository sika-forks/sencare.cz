from django import forms

from django_markdown.widgets import MarkdownWidget

from models import Comment, Article


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('author_name', 'author_email', 'title', 'text')

class ArticleAdminForm(forms.ModelForm):
    text1 = forms.CharField(widget=MarkdownWidget())
    text2 = forms.CharField(widget=MarkdownWidget())

    class Meta:
        model = Article

